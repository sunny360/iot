package com.iteaj.iot.test.client.line;

import com.iteaj.iot.client.protocol.ClientInitiativeProtocol;
import com.iteaj.iot.test.ServerInfoUtil;
import com.iteaj.iot.test.SystemInfo;
import com.iteaj.iot.test.TestProtocolType;
import com.iteaj.iot.test.message.line.LineMessageBody;
import com.iteaj.iot.test.message.line.LineMessageHead;

public class LineClientRequestProtocol extends ClientInitiativeProtocol<LineClientMessage> {

    private String deviceSn;

    public LineClientRequestProtocol(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    @Override
    protected LineClientMessage doBuildRequestMessage() {
        String messageId = ServerInfoUtil.getMessageId();
        SystemInfo systemInfo = new SystemInfo(); //ServerInfoUtil.getSystemInfo();
        LineMessageHead messageHead = LineMessageHead.buildHeader(this.deviceSn, messageId, TestProtocolType.CIReq, systemInfo);
        return new LineClientMessage(messageHead, LineMessageBody.build());
    }

    @Override
    public void doBuildResponseMessage(LineClientMessage message) {

    }

    @Override
    public TestProtocolType protocolType() {
        return TestProtocolType.CIReq;
    }
}

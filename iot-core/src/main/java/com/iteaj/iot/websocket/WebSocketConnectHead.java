package com.iteaj.iot.websocket;

import com.iteaj.iot.ProtocolType;

public class WebSocketConnectHead extends WebSocketHead {

    public WebSocketConnectHead(byte[] message) {
        super(message);
    }

    public WebSocketConnectHead(String equipCode) {
        super(equipCode);
    }
}

package com.iteaj.iot.websocket;

public interface WebSocketServerMessage extends WebSocketMessage {

    HttpRequestWrapper request();

    WebSocketMessage setRequest(HttpRequestWrapper request);
}

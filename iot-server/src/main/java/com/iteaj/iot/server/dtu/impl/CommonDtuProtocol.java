package com.iteaj.iot.server.dtu.impl;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.server.dtu.DtuCommonProtocolType;
import com.iteaj.iot.server.protocol.ServerInitiativeSyncProtocol;

import java.io.IOException;

/**
 * 使用同步请求协议
 */
public class CommonDtuProtocol extends ServerInitiativeSyncProtocol<CommonDtuServerMessage> {

    private String deviceSn;
    private byte[] message;

    public CommonDtuProtocol(String deviceSn, byte[] message) {
        this.message = message;
        this.deviceSn = deviceSn;
    }

    @Override
    protected CommonDtuServerMessage doBuildRequestMessage() throws IOException {
        DefaultMessageHead messageHead = new DefaultMessageHead(this.deviceSn, this.deviceSn, protocolType());
        return new CommonDtuServerMessage(messageHead);
    }

    @Override
    protected void doBuildResponseMessage(CommonDtuServerMessage message) {

    }

    @Override
    public ProtocolType protocolType() {
        return DtuCommonProtocolType.COMMON;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public byte[] getMessage() {
        return message;
    }

    public void setMessage(byte[] message) {
        this.message = message;
    }
}

package com.iteaj.iot.server.dtu.impl;

import com.iteaj.iot.AbstractProtocol;
import com.iteaj.iot.config.ConnectProperties;
import com.iteaj.iot.server.dtu.DtuMessageAware;
import com.iteaj.iot.server.dtu.SimpleChannelForDtuDecoderComponent;

/**
 * 通用的dtu服务组件
 */
public class CommonDtuServerComponent extends SimpleChannelForDtuDecoderComponent<CommonDtuServerMessage> {

    public CommonDtuServerComponent(ConnectProperties connectProperties) {
        super(connectProperties);
    }

    public CommonDtuServerComponent(ConnectProperties connectProperties, DtuMessageAware<CommonDtuServerMessage> dtuMessageAwareDelegation) {
        super(connectProperties, dtuMessageAwareDelegation);
    }

    @Override
    public String getDesc() {
        return "支持大部分DTU+任意协议的设备组件";
    }

    @Override
    public String getName() {
        return "通用DTU服务";
    }

    @Override
    public AbstractProtocol doGetProtocol(CommonDtuServerMessage message) {
        return remove(message.getHead().getMessageId());
    }
}

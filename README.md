### 物联网网络中间件(v2.5.4)
使用java语言且基于netty, spring boot, redis等开源项目开发来的物联网网络中间件, 支持udp, tcp通讯等底层协议和http, mqtt, websocket(默认实现和自定义协议头实现), modbus(tcp,rtu),plc,dtu(支持心跳，设备注册功能以及AT协议和自定义协议支持),dtu for modbus tcp,dtu for modbus rtu组件适配 等上层协议. 主打工业物联网底层网络交互、设备管理、数据存储、大数据处理. (其中plc包括西门子S7系列，欧姆龙Fins，罗克韦尔CIP，三菱MC). 数据存储将使用taos数据库以及redis消息队列

#### 如果您觉得的还可以点个star让更多开发者了解此项目
- [iot框架使用教程](http://doc.iteaj.com/)
- [项目演示网址(支持DTU和PLC调试)](http://iot.iteaj.com/#/login)
- [演示项目后端代码(springboot)](https://gitee.com/iteaj/iboot)
- [演示项目前端代码(vue3+antdv3+vite2)](https://gitee.com/iteaj/ivzone)
#### 合作方式(qq: 97235681), 加好友时请备注：iot合作
1. 单纯的设备协议对接(￥800+)
2. 完整的管理系统(价格详谈)
3. 设备sdk开发(￥5000+)
#### 主要特性
- 支持服务端启动监听多个端口, 统一所有协议可使用的api接口
- 包含一套代理客户端通信协议，支持调用：客户端 -> 服务端 -> 设备 -> 服务端 -> 客户端
- 支持设备协议对象和其业务对象进行分离(支持默认业务处理器【spring单例注入】和自定义业务处理器)
- 支持同步和异步调用设备, 支持应用程序代理客户端和设备服务端和设备三端之间的同步和异步调用
- 服务端支持设备上线/下线/异常的事件通知, 支持自定义心跳事件， 客户端支持断线重连
- 丰富的日志打印功能，包括设备上线，下线提示， 一个协议的生命周期(请求或者请求+响应)等
- 支持请求时如果连接断线会自动重连(同步等待成功后发送)
- 支持客户端发送请求时如果客户端不存在将自动创建客户端(同步等待成功后发送)
- 支持作为mqtt网关，将从工业物联网采集的数据更加简单方便的发布到mqtt服务器
- 支持常用的物联网协议比如：mqtt、plc、modbus、websocket
- 支持通过dtu方式使用modbus协议操作plc

#### 模拟工具
1. [QtSwissArmyKnife](https://gitee.com/qsaker/QtSwissArmyKnife) 支持udp、tcp、modbus、websocket、串口等调试
2. [IotClient](https://github.com/zhaopeiym/IoTClient) 支持plc(西门子，欧姆龙，三菱)，modbus，串口，mqtt，tcp, udp等模拟和调试
### netty使用教程
#### netty内存释放检测
1. jvm启动参数加上(不建议正式环境使用)：-Dio.netty.leakDetectionLevel=paranoid -Dio.netty.leakDetection.targetRecords=8